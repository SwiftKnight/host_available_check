#import "HostAvailableCheckPlugin.h"
#if __has_include(<host_available_check/host_available_check-Swift.h>)
#import <host_available_check/host_available_check-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "host_available_check-Swift.h"
#endif

@implementation HostAvailableCheckPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftHostAvailableCheckPlugin registerWithRegistrar:registrar];
}
@end
