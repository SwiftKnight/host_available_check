import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:host_available_check/host_available_check.dart';

void main() {
  const MethodChannel channel = MethodChannel('host_available_check');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    // expect(await HostAvailableCheck.platformVersion, '42');
  });
}
