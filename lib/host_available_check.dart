
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:host_available_check/server_host_status.dart';

class HostAvailableCheck {
  static MethodChannel _channel = new MethodChannel("io.flutter.network/status");
  static Future<List<ServerHostStatus>> check(List<String> host) async {
    if(Platform.isIOS){
      throw "IOS is UnImplement";
    }
    try {
      var result = await _channel.invokeMethod("checkNetworkStatus", host);
      if(!kReleaseMode) {
        print("==========>$result");
      }
      var response = result as List;
      return response.map((e) => ServerHostStatus.fromJson(Map<String,dynamic>.from(e))).toList();
    } on PlatformException catch (e) {
      throw e;
    }
  }
}
