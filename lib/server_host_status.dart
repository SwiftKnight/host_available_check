
part 'server_host_status.g.dart';
class ServerHostStatus{
  String host;
  String status;
  double time;
  ServerHostStatus({required this.time,required this.host,required this.status});

  factory ServerHostStatus.fromJson(Map<String, dynamic> srcJson) =>
      _$ServerHostStatusFromJson(srcJson);
  Map<String, dynamic> toJson() => _$ServerHostStatusToJson(this);
}

class ServerHostStatusEnum{
 static final String UnAvailable = "UnAvailable";
 static final String  Available = "Available";
 static final String  UnKnown ="UnKnown";
}