// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server_host_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServerHostStatus _$ServerHostStatusFromJson(Map<String, dynamic> json) {
  return ServerHostStatus(
    time: (json['time'] as num).toDouble(),
    host: json['host'] as String,
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$ServerHostStatusToJson(ServerHostStatus instance) =>
    <String, dynamic>{
      'host': instance.host,
      'status': instance.status,
      'time': instance.time,
    };
