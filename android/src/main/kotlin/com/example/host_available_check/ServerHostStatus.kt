import android.os.Handler
import android.os.Looper
import android.os.Message
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import io.flutter.plugin.common.MethodChannel.Result
class ServerHostStatusType{
    companion object instance {
        val UnAvailable:String = "UnAvailable"
        val Available:String = "Available"
        val UnKnown:String = "UnKnown"
    }
}


data class ServerHostStatus(var status:String,var host: String,var time:Double)


fun checkHostAvailable(result:Result,hosts:List<String>){
    var hostStatus = mutableListOf<Map<String,Any>>()
    var handler = object : Handler(Looper.getMainLooper()){
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            var status = msg.obj as ServerHostStatus
            hostStatus.add(mapOf("time" to status.time,"status" to status.status,"host" to status.host))
            if(hostStatus.size == hosts.size){
                result.success(hostStatus)
            }
        }
    }
    hosts.forEach{
        Thread{
            var result = ping(it)
            handler.sendMessage(Message.obtain().apply { obj=result })
        }.start()
    }
}

fun checkServerHost(host: List<String>):List<ServerHostStatus>{
    var statusResult = mutableListOf<ServerHostStatus>()
    host.forEach{e->
        var status = ping(e)
        statusResult.add(status)
    }
    return statusResult;
}

fun ping(host:String):ServerHostStatus{
    var result = ServerHostStatus(ServerHostStatusType.UnKnown,host,0.0)
    var p:Process
    try {

        p = Runtime.getRuntime().exec("ping -c 3 ${host.split("://").last()}")
        var status:Int = p.waitFor();
        var inputStream: InputStream = p.inputStream;
        var input = BufferedReader(InputStreamReader(inputStream))
        var buffer = StringBuffer()
        var line:String? = ""
        while (true){
            line = input.readLine();
            if(line == null){
                break
            }
            buffer.append("\n"+line)
        }
        if(status != 0){
            return result.apply {
                this.status = ServerHostStatusType.UnAvailable
            }
        }
//              Log.e("error","==========================>$buffer==============>$status")
        var sum = 0.0;
        if(!buffer.contains("timeout")&&!buffer.contains("localhost")) {
            for (item in buffer.split("\n").filter { str -> str.contains("time=") }
                .toList()) {
                val time = item.split("time=").last().split(" ms").first()
                sum += time.toDouble()
            }
            return result.apply {
                this.status = ServerHostStatusType.Available
                this.time = sum / 3.0
            }
        }
        return result.apply {
            this.status = ServerHostStatusType.UnAvailable
        }
    }catch (e: IOException){
        return result.apply {
            this.status = ServerHostStatusType.UnAvailable
        }
    }catch (e:InterruptedException){
        return result.apply {
            this.status = ServerHostStatusType.UnAvailable
        }
    }catch (e:Exception){
        return result.apply {
            this.status = ServerHostStatusType.UnAvailable
        }
    }
    return result
}